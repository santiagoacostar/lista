/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.Nodo;
import javax.inject.Named;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;


/**
 *
 * @author cloaiza
 */
@Named(value = "listaEstudiantesController")
@SessionScoped
public class ListaEstudiantesController implements Serializable {
    private ListaEstudiantesSE lista= new ListaEstudiantesSE();

    
    private Nodo temp;
    
    private boolean verNuevo=false;
    
    private Estudiante estudianteAdicionar;
    private final char genero='F';

    public Estudiante getEstudianteAdicionar() {
        return estudianteAdicionar;
    }

    public void setEstudianteAdicionar(Estudiante estudianteAdicionar) {
        this.estudianteAdicionar = estudianteAdicionar;
    }
    
    

    public boolean isVerNuevo() {
        return verNuevo;
    }

    public void setVerNuevo(boolean verNuevo) {
        this.verNuevo = verNuevo;
    }
    
    

    public Nodo getTemp() {
        return temp;
    }

    public void setTemp(Nodo temp) {
        this.temp = temp;
    }
    
    
    
    private String listado;

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }
    
    
    
    public ListaEstudiantesSE getLista() {
        return lista;
    }

    public void setLista(ListaEstudiantesSE lista) {
        this.lista = lista;
    }
    public void mostrarListado()
    {
        listado= lista.listarNodos();
    }
    
    
        /**
     * Creates a new instance of ListaEstudiantesController
     */
    public ListaEstudiantesController() {
        adicionarEstudiante("Laura", 20, "Caldas", genero);

        adicionarEstudiante("Sofia", 21, "Manizales", genero);

        adicionarEstudiante("Geraldine", 20, "Autonoma", genero);

        adicionarEstudiante("Claudia", 27, "Catolica", genero);
        //mostrarListado();
        
        temp= lista.getCabeza();
    }
    
    
    public void adicionarEstudiante(String nombre, int edad, String universidad, char genero)
    {
        Estudiante estu= new Estudiante(nombre, edad, universidad, genero);
        
        lista.adicionarNodo(estu);
        
    }        
    
    
     public void adicionarEstudianteAlInicio(String nombre, int edad, String universidad, char genro)
    {
        Estudiante estu= new Estudiante(nombre, edad, universidad, genero);
        
        lista.adicionarNodoInicial(estu);
        
    }  
     
     
     public void irAlsiguiente()
     {
         if(temp.getSiguiente()!=null)
         {
             temp=temp.getSiguiente();
         }
     }
     
     
     public void anterior()
     {
         //Imposible
     }
     
     public void irAlPrimero()
     {
         temp=lista.getCabeza();
     }
     
     
      public void irAlUltimo()
     {
         Nodo ultimo= lista.obtenerUltimoNodo();
         if(ultimo!=null)
         {
            temp=ultimo;
         }   
     }
     public void verCrearEstudiante()
     {
         verNuevo=true;
         estudianteAdicionar= new Estudiante();
     }
     
     public void adicionarAlInicio()
     {
         lista.adicionarNodoInicial(estudianteAdicionar);
         irAlPrimero();
         verNuevo=false;
     }
     
      public void adicionarAlFinal()
     {
         lista.adicionarNodo(estudianteAdicionar);
         irAlPrimero();
         verNuevo=false;
     }
      public void eliminarNodo(){
          lista.eliminarNodo(temp.getDato());
          irAlPrimero();                    
      }
      public void invertirLista(){
        lista.invertirNodo();
        irAlPrimero();
    
}
      public void verEliminarEstudiantes(){
          verNuevo=true;
      }
      public void eliminarPosicion(){
          lista.eliminarPosicion();
          irAlPrimero();
          lista.setPosicion(0);
          verNuevo=false;
      }
      public void encontrarEdad(){
          lista.ordenarPorEdades();
          irAlPrimero();
      }
      public void verSuperiores(){
          verNuevo=true;
      }
      public void mayoresAlNumero(){
         lista.edadesMayor();
         irAlPrimero();
}
      
      
      
     
}
