/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.NodoDE;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
@Named(value = "listaEstudiantesControllerDE")
@SessionScoped
public class ListaEstudiantesControllerDE implements Serializable {

    private ListaEstudianteDE lista = new ListaEstudianteDE();
    private ListaEstudiantesSE control1=new ListaEstudiantesSE();
    
    private int posicion;
    private NodoDE temp;
    private Nodo temp2;

    private boolean verNuevo = false;

    private Estudiante estudianteAdicionar;
    private final char genero='M';

    public Estudiante getEstudianteAdicionar() {
        return estudianteAdicionar;
    }

    public void setEstudianteAdicionar(Estudiante estudianteAdicionar) {
        this.estudianteAdicionar = estudianteAdicionar;
    }

    public boolean isVerNuevo() {
        return verNuevo;
    }

    public void setVerNuevo(boolean verNuevo) {
        this.verNuevo = verNuevo;
    }

    public NodoDE getTemp() {
        return temp;
    }

    public void setTemp(NodoDE temp) {
        this.temp = temp;
    }

    private String listado;

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }

    public ListaEstudianteDE getLista() {
        return lista;
    }

    public void setLista(ListaEstudianteDE lista) {
        this.lista = lista;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public void mostrarListado() {

        listado = lista.listarNodos();
    }

    public ListaEstudiantesControllerDE() {
        adicionarEstudiante("Mauricio López", 20, "Manizales", genero);

        adicionarEstudiante("Junior Celis", 21, "Catolica", genero);

        adicionarEstudiante("Sebastián", 20, "Nacional", genero);

        adicionarEstudiante("Marlon", 27, "Manizales", genero);
        mostrarListado();

        temp = lista.getCabeza();
    }

    public void adicionarEstudiante(String nombre, int edad, String universidad, char genero) {
        Estudiante estu = new Estudiante(nombre, edad, universidad, genero );

        lista.adicionarNodo(estu);

    }

    public void adicionarEstudianteAlInicio(String nombre, char genero,
            int edad, String universidad) {
        Estudiante estu = new Estudiante(nombre, edad, universidad, genero);

        lista.adicionarNodoInicialDE(estu);

    }

    public void irAlsiguiente() {
        if (temp.getSiguiente() != null) {
            temp = temp.getSiguiente();
        }
    }

    public void anterior() {
        if (temp.getAnterior() != null) {
            temp = temp.getAnterior();
        }
    }

    public void irAlPrimero() {
        temp = lista.getCabeza();
    }

    public void irAlUltimo() {
        NodoDE ultimo = lista.obtenerUltimoNodo();
        if (ultimo != null) {
            temp = ultimo;
        }
    }

    public void verCrearEstudiante() {
        verNuevo = true;
        estudianteAdicionar = new Estudiante();
    }

    public void adicionarAlInicio() {
        lista.adicionarNodoInicialDE(estudianteAdicionar);
        irAlPrimero();
        verNuevo = false;
    }

    public void adicionarAlFinal() {
        lista.adicionarNodo(estudianteAdicionar);
        irAlPrimero();
        verNuevo = false;
    }

    public void eliminarNodo() {
        lista.eliminarNodo(temp.getDato());
        irAlPrimero();
    }

    public void invertirLista() {
        lista.invertirNodo();
        irAlPrimero();

    }

    public void verEliminarEstudiantes() {
        verNuevo = true;
    }

    public void eliminarPosicion() {
        lista.eliminarPosicion(posicion);
        irAlPrimero();
        lista.setPosicion(0);
        verNuevo = false;
    }

    public void encontrarEdad() {
        lista.ordenarPorEdades();
        irAlPrimero();
    }

    public void verSuperiores() {
        verNuevo = true;
    }

    public void mayoresAlNumero() {
        lista.edadesMayor();
        irAlPrimero();
    }

    public void ordenarImpares() {
        lista.ordenarImpares();
        irAlPrimero();
    }

    public String autenticarUsuario() {
        if (verNuevo) {

            return "case1";
        } else {
            return "case2";
        }
    }

}
