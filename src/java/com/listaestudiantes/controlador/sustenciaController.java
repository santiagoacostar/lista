/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.NodoDE;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.NodoDE;
import com.listaestudiantes.pojo.Nodo;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * /**
 *
 * @author santi
 */
@Named(value = "sustenciaController")
@SessionScoped
public class sustenciaController implements Serializable{

    ListaEstudiantesController control1 = (ListaEstudiantesController) FacesUtils.getManagedBean("listaEstudiantesController");
    ListaEstudiantesControllerDE control2 = (ListaEstudiantesControllerDE) FacesUtils.getManagedBean("listaEstudiantesControllerDE");
    private NodoDE temp;
    private String listado;

    public NodoDE getTemp() {
        return temp;
    }

    public void setTemp(NodoDE temp) {
        this.temp = temp;
    }
//

    public void mostrarListado() {
        Nodo temp2 = control1.getLista().getCabeza();
        while (temp2 != null) {
            control2.getLista().adicionarNodo(temp2.getDato());
            temp2 = temp2.getSiguiente();
        }
    }

    public sustenciaController() {
        mostrarListado();
        temp = control2.getLista().getCabeza();
    }

//      public String getListado() {
//        return listado;
//    }
//
//    public void setListado(String listado) {
//        this.listado = listado;
//    }
//
//    
//   
//
//    public void irAlPrimero() {
//        temp=control2.getLista().getCabeza();
//    }
//
    public NodoDE obtenerHombres() {
        temp = control2.getLista().getCabeza();
        ListaEstudianteDE listaHombres = new ListaEstudianteDE();
        int cont = 1;
        while (cont <= control2.getLista().contarNodos()) {
            if (temp.getDato().getGenero() == 'M') {
                NodoDE nuevo = new NodoDE(temp.getDato());
                listaHombres.adicionarNodo(nuevo.getDato());
            }
            cont++;
            temp = temp.getSiguiente();
        }
        return listaHombres.getCabeza();
    }

    public NodoDE obtenerMujeres() {
        temp = control2.getLista().getCabeza();
        ListaEstudianteDE listaMujeres = new ListaEstudianteDE();
        int cont = 1;
        while (cont <= control2.getLista().contarNodos()) {
            if (temp.getDato().getGenero() == 'F') {
                NodoDE nuevo = new NodoDE(temp.getDato());
                listaMujeres.adicionarNodo(nuevo.getDato());
            }
            cont++;
            temp = temp.getSiguiente();
        }
        return listaMujeres.getCabeza();
    }

    public void unirListas() {
        ListaEstudianteDE listaHombres = new ListaEstudianteDE();
        ListaEstudianteDE listaMujeres = new ListaEstudianteDE();
        listaHombres.setCabeza(obtenerHombres());
        listaMujeres.setCabeza(obtenerMujeres());
        int maximo;

        if (listaMujeres.contarNodos() >= listaHombres.contarNodos()) {
            maximo = listaMujeres.contarNodos();
        } else {
            maximo = listaHombres.contarNodos();
        }

        ListaEstudianteDE listaResultado = new ListaEstudianteDE();
        listaHombres.ordenarPorEdades();
        listaMujeres.ordenarPorEdades();
        listaMujeres.invertirNodo();

        NodoDE temp2;
        temp = listaHombres.getCabeza();
        temp2 = listaMujeres.getCabeza();

        if (listaHombres.contarNodos() > listaMujeres.contarNodos()) {
            for (int i = 1; i <= maximo; i++) {
                if (temp2 != null) {
                    listaResultado.adicionarNodo(temp.getDato());
                    listaResultado.adicionarNodo(temp2.getDato());
                    temp = temp.getSiguiente();
                    temp2 = temp2.getSiguiente();
                } else {
                    listaResultado.adicionarNodo(temp.getDato());
                    temp = temp.getSiguiente();
                }
            }

        } else if (listaHombres.contarNodos() < listaMujeres.contarNodos()) {
            for (int i = 1; i <= maximo; i++) {
                if (temp != null) {
                    listaResultado.adicionarNodo(temp.getDato());
                    listaResultado.adicionarNodo(temp2.getDato());
                    temp = temp.getSiguiente();
                    temp2 = temp2.getSiguiente();
                } else {
                    listaResultado.adicionarNodo(temp.getDato());
                    temp = temp.getSiguiente();
                }
            }
        } else {
            for (int i = 1; i <= maximo; i++) {
                if (temp != null) {
                    listaResultado.adicionarNodo(temp.getDato());
                    listaResultado.adicionarNodo(temp2.getDato());
                    temp = temp.getSiguiente();
                    temp2 = temp2.getSiguiente();

                } else {
                    listaResultado.adicionarNodo(temp2.getDato());
                }

            }
        }

        control2.getLista().setCabeza(listaResultado.getCabeza());
        irAlPrimero();

    }

    public Estudiante mujerMenor() {
        Nodo temp2 = control1.getLista().getCabeza();
        Nodo nodoMenor = control1.getLista().getCabeza();
        int menor = control1.getLista().getCabeza().getDato().getEdad();
        while (temp2 != null) {
            if (temp2.getDato().getEdad() <= menor) {
                menor = temp2.getDato().getEdad();
                nodoMenor = temp2;
            }
            temp2 = temp2.getSiguiente();
        }
        return nodoMenor.getDato();
    }

    public int mujerMayor() {
        Nodo temp2 = control1.getLista().getCabeza();
        int mujerMayor = control1.getLista().getCabeza().getDato().getEdad();
        while (temp2 != null) {
            if (temp2.getDato().getEdad() >= mujerMayor) {
                mujerMayor = temp2.getDato().getEdad();
            }
            temp2 = temp2.getSiguiente();
        }
        return mujerMayor;
    }

    public Estudiante hombreMayor() {
        temp = obtenerHombres();
        NodoDE nodoMayor = control2.getLista().getCabeza();
        int mayor = control2.getLista().getCabeza().getDato().getEdad();
        while (temp != null) {
            if (temp.getDato().getEdad() >= mayor) {
                mayor = temp.getDato().getEdad();
                nodoMayor = temp;
            }
            temp = temp.getSiguiente();
        }
        return nodoMayor.getDato();
    }

    public int hombreMenor() {
        temp = control2.getLista().getCabeza();
        int hombreMenor = control2.getLista().getCabeza().getDato().getEdad();
        while (temp != null) {
            if (temp.getDato().getEdad() <= hombreMenor) {
                hombreMenor = temp.getDato().getEdad();
            }
            temp = temp.getSiguiente();
        }
        return hombreMenor;
    }

    public int diferenciarEdad() {
        return mujerMayor() - hombreMenor();
    }

    public int promediarMujeres() {
        Nodo temp2 = control1.getLista().getCabeza();
        int promedio = 0;
        while (temp2 != null) {
            promedio = promedio + temp2.getDato().getEdad();
            temp2 = temp2.getSiguiente();
        }
        return promedio / control1.getLista().contarNodos();
    }

    public int promediarHombres() {
        temp = control2.getLista().getCabeza();
        int promedio = 0;
        while (temp != null) {
            promedio = promedio + temp.getDato().getEdad();
            temp = temp.getSiguiente();
        }
        return promedio / control1.getLista().contarNodos();
    }

    public int promediarEdades() {
        return (promediarHombres() + promediarMujeres() / 2);
    }

    public void ordenarAlfabeticamente() {
        ListaEstudianteDE listaResultado = new ListaEstudianteDE();
        int maximo = control2.getLista().contarNodos();
        for (int i = 1; i <= maximo; i++) {
            temp = control2.getLista().getCabeza();
            if (temp.getSiguiente() != null) {
                NodoDE alf = temp.getSiguiente();
                do {
                    int comparador = temp.getDato().getUniversidad().compareToIgnoreCase(alf.getDato().getUniversidad());
                    if (comparador < 0) {
                        NodoDE nodoUno = temp;
                    } else if (comparador > 0) {
                        temp = alf;
                        NodoDE nodoUno = temp;
                    }
                    alf = alf.getSiguiente();
                } while (alf != null);
                listaResultado.adicionarNodo(temp.getDato());
                control2.getLista().eliminarNodo(temp.getDato());
            } else {
                listaResultado.adicionarNodo(temp.getDato());
            }
        }
        control2.getLista().setCabeza(listaResultado.getCabeza());
        irAlPrimero();
    }

    public void irAlsiguiente() {
        if (temp.getSiguiente() != null) {
            temp = temp.getSiguiente();
        }
    }

    public void irAlAnterior() {
        if (temp.getAnterior() != null) {
            temp = temp.getAnterior();
        }
    }

    public void irAlPrimero() {
        temp = control2.getLista().getCabeza();
    }

    public void irAlUltimo() {
        NodoDE ultimo = control2.getLista().obtenerUltimoNodo();
        if (ultimo != null) {
            temp = ultimo;
        }
    }
}
