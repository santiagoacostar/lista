/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.Nodo;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class ListaEstudiantesSE implements Serializable {

    private Nodo cabeza;
    private int posicion;
    private int edadMayor;

    public int getEdadMayor() {
        return edadMayor;
    }

    public void setEdadMayor(int edadMayor) {
        this.edadMayor = edadMayor;
    }

    public ListaEstudiantesSE() {
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }
    

    public String adicionarNodo(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new Nodo(info);

        } else {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            //Parado en el último  
            temp.setSiguiente(new Nodo(info));
        }
        return "Adicionado con éxito";

    }

    public String adicionarNodoInicial(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new Nodo(info);

        } else {
            Nodo temp = new Nodo(info);
            temp.setSiguiente(cabeza);
            cabeza = temp;
        }
        return "Adicionado con éxito";

    }

    public String listarNodos() {
        String listado = "";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();
            }
            return listado;
        }

    }

    public int contarNodos() {
        if (cabeza == null) {
            return 0;
        } else {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();
            }
            return cont;
        }
    }

    public Nodo obtenerUltimoNodo() {
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }

    public String eliminarNodo(Estudiante dato) {

        if (cabeza == null) {
            return "la lista esta vacia";

        }
        if (cabeza.getDato().equals(dato)) {
            cabeza = cabeza.getSiguiente();
            return "se ha eliminado";

        } else {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                if (temp.getSiguiente().getDato().equals(dato)) {
                    temp.setSiguiente(temp.getSiguiente().getSiguiente());
                    break;

                }
                temp = temp.getSiguiente();
            }

            return "no se encontro";
        }
    }

    public String invertirNodo() {
        ListaEstudiantesSE lista2 = new ListaEstudiantesSE();
        if (cabeza != null) {

            Nodo temp = cabeza;
            while (temp != null) {
                lista2.adicionarNodoInicial(temp.getDato());
                temp = temp.getSiguiente();
            }
            cabeza = lista2.cabeza;
        }
        return "lista invertida";
    }

    public int eliminarPosicion() {
        if (cabeza != null) {
            if (getPosicion() == 1) {
                cabeza = cabeza.getSiguiente();
            } else if (getPosicion() <= contarNodos()&& getPosicion()>0 ) {
                Nodo temp = cabeza;
                int cont = 1;
                while (cont < (getPosicion() - 1)) {
                    temp = temp.getSiguiente();
                    cont++;
                }

                temp.setSiguiente(temp.getSiguiente().getSiguiente());

            }
        }
        return 0;
    }

    public int encontrarMenor() {
        Nodo temp = cabeza;
        int cont = 1;
        int menor;
        menor = cabeza.getDato().getEdad();
        posicion = 1;
        while (temp != null) {
            if (temp.getDato().getEdad() <= menor) {
                menor = temp.getDato().getEdad();
                posicion = cont;
            }
            temp = temp.getSiguiente();
            cont++;

        }
        return posicion;
    }

    public Estudiante obtenerPosicion(int posicion) {
        Nodo temp = cabeza;
        int cont = 1;
        while (cont < posicion) {
            temp = temp.getSiguiente();
            cont++;
        }
        return temp.getDato();
    }

    public void ordenarPorEdades() {
        ListaEstudiantesSE lista2 = new ListaEstudiantesSE();
        int posicion = 0;

        while (this.cabeza != null) {
            posicion = this.encontrarMenor();
            Estudiante estudianteMenor = this.obtenerPosicion(posicion);
            this.eliminarPosicion();
            lista2.adicionarNodo(estudianteMenor);
        }

        this.cabeza = lista2.cabeza;
    }

    public void edadesMayor() {
        ListaEstudiantesSE lista2 = new ListaEstudiantesSE();
        Nodo temp = cabeza;
        while (temp != null) {
            if (getEdadMayor() <= temp.getDato().getEdad()) {
                lista2.adicionarNodo(temp.getDato());
            }
            temp = temp.getSiguiente();
        }
        cabeza = lista2.cabeza;
    }

}
