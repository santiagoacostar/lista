/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.NodoDE;
import com.listaestudiantes.pojo.Nodo;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author santi
 */
@Named(value = "ListaEstudiantesControllerDESE")
@SessionScoped
public class ListaEstudiantesControllerDESE implements Serializable {

    ListaEstudiantesController control1 = (ListaEstudiantesController) FacesUtils.getManagedBean("listaEstudiantesController");
    ListaEstudiantesControllerDE control2 = (ListaEstudiantesControllerDE) FacesUtils.getManagedBean("listaEstudiantesControllerDE");

    private String listado;
    private NodoDE temp;

    public NodoDE getTemp() {
        return temp;
    }

    public void setTemp(NodoDE temp) {
        this.temp = temp;
    }

    public void mostrarListado() {
        Nodo temp2 = control1.getLista().getCabeza();
        while (temp2 != null) {
            control2.getLista().adicionarNodo(temp2.getDato());
            temp2 = temp2.getSiguiente();
        }
        listado = control2.getLista().listarNodos();
    }

    public ListaEstudiantesControllerDESE() {
        mostrarListado();
        temp = control2.getLista().getCabeza();
    }

    public void irAlsiguiente() {
        if (temp.getSiguiente() != null) {
            temp = temp.getSiguiente();
        }
    }

    public void anterior() {
        if (temp.getAnterior() != null) {
            temp = temp.getAnterior();
        }
    }
    private Nodo cabeza;
    private NodoDE cabeza2;

    public void intercalar() {
        ListaEstudianteDE ListaResultante = new ListaEstudianteDE();

        control1.encontrarEdad();
        control2.encontrarEdad();

        int maximo = 0;
        if (control1.getLista().contarNodos() >= control2.getLista().contarNodos()) {
            maximo = control1.getLista().contarNodos();
        } else {
            maximo = control2.getLista().contarNodos();
        }

        Nodo temp = control1.getLista().getCabeza();

        for (int i = 0; i < maximo; i++) {
            if (temp != null) {
                ListaResultante.adicionarNodo(temp.getDato());
                temp = temp.getSiguiente();
            }
            if (temp != null) {
                ListaResultante.adicionarNodo(temp.getDato());
                temp = temp.getSiguiente();
            }

        }
        cabeza2 = ListaResultante.getCabeza();

    }

    public void irAlSiguiente() {
        if (cabeza2.getSiguiente() != null) {
            cabeza2 = cabeza2.getSiguiente();

        }
    }

   // public void anterior() {
     //   if (temp2.getAnterior() != null) {
   //         temp2 = temp2.getAnterior();
     //   }
   // }

    public void irAlPrimero() {
        temp = cabeza2;
    }

    //public void mostrarListado(){
      //  Nodo tempS= control1.getLista().getCabeza();
       // while(tempS != null){
            
        //}
    //}
    
    
    public int diferenciaDeEdad() {
        control1.encontrarEdad();        
        control2.encontrarEdad();
        int resta = 0;
        if (cabeza != null) {
            if (cabeza2 != null) {
                resta = cabeza.getDato().getEdad() - cabeza2.getDato().getEdad();
            }
        }
        return resta;
    }

    public int PromedioEdad() {
        control1.encontrarEdad();        
        control2.encontrarEdad();
        int prom = 0;
        int suma = 0;
        
        if (cabeza != null) {
            if (cabeza2 != null) {
                
                
                
            }
        }
        if (suma != 0) {
            prom = suma / 2;
        }
        return prom;
    }

    public void ordenarABC() {
        ListaEstudianteDE alfabero = new ListaEstudianteDE();
        int mox = control2.getLista().contarNodos();
        for (int j = 0; j < mox; j++) {
            temp = control2.getLista().getCabeza();
            if (temp.getSiguiente() != null) {
                NodoDE puntero = temp.getSiguiente();
                while (puntero != null) {
                    int comp = temp.getDato().getUniversidad().compareTo(puntero.getDato().getUniversidad());
                    if (comp > 0) {
                        NodoDE nodoMenor = temp;
                    } else if (comp < 0) {
                        temp = puntero;
                        NodoDE nodoMenor = temp;
                    }
                    puntero=puntero.getSiguiente();
                }
                alfabero.adicionarNodo(temp.getDato());
                control2.getLista().eliminarNodo(temp.getDato());

            }else {
                alfabero.adicionarNodo(temp.getDato());
            }

        }
        control2.getLista().setCabeza(alfabero.getCabeza());
    }
}
