/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.NodoDE;
import java.io.Serializable;

/**
 *
 * @author santi
 */
public class ListaEstudianteDE implements Serializable {

    private NodoDE cabeza;
    private int posicion;
    private int edadMayor;

    public int getEdadMayor() {
        return edadMayor;
    }

    public void setEdadMayor(int edadMayor) {
        this.edadMayor = edadMayor;
    }

    public ListaEstudianteDE() {
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public NodoDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoDE cabeza) {
        this.cabeza = cabeza;
    }

    public String adicionarNodo(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new NodoDE(info);

        } else {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            //Parado en el último  
            temp.setSiguiente(new NodoDE(info));
            temp.getSiguiente().setAnterior(temp);

        }
        return "Adicionado con éxito";

    }

    public String adicionarNodoInicialDE(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new NodoDE(info);

        } else {
            NodoDE temp = new NodoDE(info);
            temp.setSiguiente(cabeza);
            cabeza.setAnterior(temp);
            cabeza = temp;
        }
        return "Adicionado con éxito";

    }

    public String listarNodos() {
        String listado = "";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            NodoDE temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();
            }
            return listado;
        }
    }

    public int contarNodos() {
        if (cabeza == null) {
            return 0;
        } else {
            NodoDE temp = cabeza;
            int cont = 0;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();
            }
            return cont;
        }
    }

    public NodoDE obtenerUltimoNodo() {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }

    public String eliminarNodo(Estudiante dato) {

        if (cabeza == null) {
            return "la lista esta vacia";

        } else if (cabeza.getDato().equals(dato)) {
            cabeza = cabeza.getSiguiente();
            cabeza.setAnterior(null);
            return "se ha eliminado";

        } else if (obtenerUltimoNodo().getDato().equals(dato)) {
            obtenerUltimoNodo().getAnterior().setSiguiente(null);
            obtenerUltimoNodo().setAnterior(null);
        } else {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
                if (temp.getDato().equals(dato)) {
                    temp.getAnterior().setSiguiente(temp.getSiguiente());
                    temp.getSiguiente().setAnterior(temp.getAnterior());

                }

            }

        }
        return "no se encontro";
    }

    public String invertirNodo() {
        ListaEstudianteDE lista2 = new ListaEstudianteDE();
        if (cabeza != null) {

            NodoDE temp = cabeza;
            while (temp != null) {
                lista2.adicionarNodoInicialDE(temp.getDato());
                temp = temp.getSiguiente();
            }
            cabeza = lista2.cabeza;
        }

        return "lista invertida";
    }

    public void eliminarPosicion(int pos) {
        if (cabeza != null) {
            if (getPosicion() == 1) {
                if (cabeza.getSiguiente() == null) {
                    cabeza = null;
                } else {
                    cabeza = cabeza.getSiguiente();
                    cabeza.setAnterior(null);
                }
            } else if (getPosicion() <= contarNodos()) {
                NodoDE temp = cabeza;
                int cont = 1;
                while (cont < getPosicion()) {
                    temp = temp.getSiguiente();
                    cont++;
                }
                if (temp.equals(obtenerUltimoNodo())) {
                    obtenerUltimoNodo().getAnterior().setSiguiente(null);
                    obtenerUltimoNodo().setAnterior(null);

                } else {
                    temp.getAnterior().setSiguiente(temp.getSiguiente());
                    temp.getSiguiente().setAnterior(temp.getAnterior());
                }
            }
        }

    }

    public int encontrarMenor() {
        NodoDE temp = cabeza;
        int cont = 1;
        int menor;
        menor = cabeza.getDato().getEdad();
        posicion = 1;
        while (temp != null) {
            if (temp.getDato().getEdad() <= menor) {
                menor = temp.getDato().getEdad();
                posicion = cont;
            }
            temp = temp.getSiguiente();
            cont++;

        }
        return posicion;
    }

    public int encontrarMayor() {
        NodoDE temp = cabeza;
        int cont = 1;
        int menor;
        menor = cabeza.getDato().getEdad();
        posicion = 1;
        while (temp != null) {
            if (temp.getDato().getEdad() >= menor) {
                menor = temp.getDato().getEdad();
                posicion = cont;
            }
            temp = temp.getSiguiente();
            cont++;

        }
        return posicion;
    }

    public Estudiante obtenerPosicion(int posicion) {
        NodoDE temp = cabeza;
        int cont = 1;
        while (cont < posicion) {
            temp = temp.getSiguiente();
            cont++;
        }
        return temp.getDato();
    }

    public void ordenarPorEdades() {
        ListaEstudianteDE lista2 = new ListaEstudianteDE();

        while (this.cabeza != null) {
            posicion = this.encontrarMayor();
            Estudiante estudianteMayor = this.obtenerPosicion(posicion);
            this.eliminarPosicion(posicion);
            lista2.adicionarNodo(estudianteMayor);
        }

        this.cabeza = lista2.cabeza;
    }

    public void edadesMayor() {
        ListaEstudianteDE lista2 = new ListaEstudianteDE();
        NodoDE temp = cabeza;
        while (temp != null) {
            if (getEdadMayor() <= temp.getDato().getEdad()) {
                lista2.adicionarNodo(temp.getDato());
            }
            temp = temp.getSiguiente();
        }
        cabeza = lista2.cabeza;
    }

    public NodoDE obtenerListaPares() {
        ListaEstudianteDE listatemp = new ListaEstudianteDE();
        NodoDE temp = cabeza;
        int cont = 1;
        while (temp != null) {
            if (cont % 2 == 0) {
                NodoDE nodoPar = new NodoDE(temp.getDato());
                listatemp.adicionarNodo(nodoPar.getDato());
            }
            cont = cont + 1;
            temp = temp.getSiguiente();

        }
        return listatemp.cabeza;
    }

    public NodoDE obtenerListaImpar() {
        ListaEstudianteDE listaImpar = new ListaEstudianteDE();
        NodoDE temp = cabeza;
        int cont = 1;
        while (temp != null) {
            if (cont % 2 != 0) {
                NodoDE nodoImpar = new NodoDE(temp.getDato());
                listaImpar.adicionarNodo(nodoImpar.getDato());
            }
            cont = cont + 1;
            temp = temp.getSiguiente();

        }
        return listaImpar.cabeza;
    }

    public void ordenarImpares() {

        ListaEstudianteDE listaImpar = new ListaEstudianteDE();
        ListaEstudianteDE ListaPar = new ListaEstudianteDE();
        listaImpar.setCabeza(obtenerListaImpar());
        ListaPar.setCabeza(obtenerListaPares());
        NodoDE temp = ListaPar.cabeza;
        while (temp != null) {
            listaImpar.adicionarNodo(temp.getDato());
            temp = temp.getSiguiente();
        }
        cabeza = listaImpar.getCabeza();
    }

}
