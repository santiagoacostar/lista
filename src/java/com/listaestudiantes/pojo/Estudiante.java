/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.pojo;

import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class Estudiante implements Serializable {
    
    private String nombre;
    private int edad;
    private String universidad;
    private char genero;

    public Estudiante(String nombre, int edad, String universidad, char genero) {
        this.nombre = nombre;
        this.edad = edad;
        this.universidad = universidad;
        this.genero = genero;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }
    

    public Estudiante() {
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "nombre=" + nombre + ", edad=" + edad + ", universidad=" + universidad + ", genero=" + genero + '}';
    }
    

    

    
    
    
}
